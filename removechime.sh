#! /usr/bin/env bash

set -e

line='set bell-style none'
file=/etc/inputrc

if grep -q "$line" $file
then
  sudo sed -i "/$line/s/# //g" $file
else
  echo $line | sudo tee --append $file 
fi

echo "Chime removed"
