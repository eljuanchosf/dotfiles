#! /usr/bin/env bash

set -e

group=docker

sudo apt-get install apt-transport-https ca-certificates curl software-properties-common -y -qq
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" 
sudo apt-get update -qq
sudo apt-get install docker-ce docker-compose -y -qq
if grep -q $group /etc/group
then
  echo "Group $group exists"
else
  sudo groupadd $group
fi
sudo usermod -aG $group $USER
if [ -f $HOME/.docker ]; then
  sudo chown "$USER":"$USER" $HOME/.docker -R
  sudo chmod g+rwx "$HOME/.docker" -R
fi
echo "Please restart your computer for changes to take effect"
