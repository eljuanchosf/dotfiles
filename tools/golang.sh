#! /usr/bin/env bash

set -e


if hash gvm 2>/dev/null; then
  echo "gvm already installed"
else
  sudo apt-get install curl git mercurial make binutils bison gcc build-essential -y -qq
  bash < <(curl -s -S -L https://raw.githubusercontent.com/e-nikolov/gvm/master/binscripts/gvm-installer)
  source $HOME/.gvm/scripts/gvm

  gvm install go1.4 -B
  gvm use go1.4
  gvm install go1.9.2
  gvm use go1.9.2

fi

