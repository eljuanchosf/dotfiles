#! /usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

set -e

sudo apt-get remove tmux --purge -y -qq

if hash tmux 2>/dev/null; then
  echo "Tmux is already installed"
else
  config_dir=$(cd $DIR/../home; pwd)
  if [ ! -f "$config_dir/.tmux.conf" ]; then
  	ln -s $config_dir/.tmux.conf $HOME/.tmux.conf
  fi
  mkdir -p $HOME/tmp

  sudo apt-get install automake build-essential pkg-config libevent-dev libncurses5-dev bison byacc -y -qq
  if [ ! -d "$HOME/tmp/tmux" ]; then

  	git clone https://github.com/tmux/tmux.git $HOME/tmp/tmux
  fi
  pushd $HOME/tmp/tmux
  sh ./autogen.sh
  ./configure && make
  sudo make install
  popd
  rm -rf $HOME/tmp/tmux
fi
