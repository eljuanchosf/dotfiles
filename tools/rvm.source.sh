#! /usr/bin/env bash

set -e

gpg_server="hkp://pool.sks-keyservers.net:80"

if hash rvm 2>/dev/null; then
  echo "RVM already installed"
else
  echo progress-bar >> ~/.curlrc
  sudo apt-get install dirmngr gnupg2 -y -qq
  gpg --keyserver $gpg_server --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
  curl -sSL https://get.rvm.io | bash -s stable --ruby
  source $HOME/.rvm/scripts/rvm
  rvm autolibs enable
fi
