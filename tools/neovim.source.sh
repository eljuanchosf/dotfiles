#! /usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
set -e

if hash nvim 2>/dev/null; then
  echo "Neovim already installed"
else

  mkdir $HOME/.config/nvim -p
  
  config_dir=$(cd $DIR/../home; pwd)
  if [ ! -d "$HOME/.config/nvim/" ]; then
    ln -s $config_dir/.config/nvim/init.vim $HOME/.config/nvim/
  fi
  if [ ! -d "$HOME/.config/nvim/snippets" ]; then
    ln -s $config_dir/.config/nvim/snippets $HOME/.config/nvim/snippets
  fi
  
  sudo apt-get install -y -qq python3 python3-pip ninja-build libtool \
                              libtool-bin autoconf automake cmake g++ \
                              pkg-config gettext gettext-base unzip
  mkdir -p $HOME/tmp

  pushd $HOME/tmp
  if [ ! -d neovim ]; then
    git clone https://github.com/neovim/neovim.git
  fi

  cd neovim
  make
  sudo make install
  popd

  sudo pip3 install --upgrade neovim

  curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

  echo "neovim has been installed"
fi
