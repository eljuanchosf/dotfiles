#! /usr/bin/env bash

set -e

if hash terraform 2>/dev/null; then
  echo "Terraform already installed"
else

  sudo apt-get install -y unzip wget
  mkdir -p $HOME/tmp

  pushd $HOME/tmp
  wget -O terraform.zip https://releases.hashicorp.com/terraform/0.11.4/terraform_0.11.4_linux_amd64.zip
  unzip terraform.zip
  sudo mv terraform /usr/local/bin/
  rm terraform.zip 
  popd

  echo "Terraform has been installed"
fi
