# Box Setup

1. Get `vim-plug`:
```
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```
2. Copy files from `home/*` to your `$HOME` directory.
3. Open NeoVim and run `:PlugInstall`