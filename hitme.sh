#! /usr/bin/env bash

set -e

source removechime.sh
source addbashcustomizations.sh
source installfiracode.sh
pushd ./tools/
source golang.sh
source tmux.source.sh
source neovim.source.sh
source rvm.source.sh
popd
