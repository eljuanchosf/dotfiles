#! /usr/bin/env bash

set -e

line='source $HOME/.bash_customizations'
file="$HOME/.bashrc"

cp home/.bash_customizations $HOME/

if grep -q "$line" $file
then
  echo ".bash_customizations already there!"
else
  echo $line >> $file
  echo "Bash customizations added. Don't forget to run: '$ source $HOME/.bashrc'" 
fi
