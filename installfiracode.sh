#! /usr/bin/env bash

set -e

if fc-list | grep --quiet "Fira Code"; then
  echo "Fira Code already installed"
else
  if hash fc-cache 2>/dev/null; then
    sudo apt-get install wget -y -qq
    mkdir -p $HOME/.local/share/fonts
    for type in Bold Light Medium Regular Retina; do
      wget -O ~/.local/share/fonts/FiraCode-${type}.ttf \
        "https://github.com/tonsky/FiraCode/blob/master/distr/ttf/FiraCode-${type}.ttf?raw=true";
    done
    fc-cache -f
  else
    echo "No fc-cache installed. Might be a terminal only system?"
  fi
fi

